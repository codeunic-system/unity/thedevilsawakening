﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pot : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Smash()
    {
        this.anim.SetBool("Smash", true);
        StartCoroutine(BreakGo());
    }

    IEnumerator BreakGo()
    {
        yield return new WaitForSeconds(0.3f);
        gameObject.SetActive(false);
    }

}
