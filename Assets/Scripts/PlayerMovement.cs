﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D myRigidbody;
    private Vector3 change;
    private Animator animator;

    public float speed = 4.0f;
    public PlayerState currentState;
    public FloatValue currentHealth;
    public Signal playerHealhSignal;

    private void Start()
    {
        currentState = PlayerState.WALK;
        myRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animator.SetFloat("MoveX", 0);
        animator.SetFloat("MoveY", 0);
    }

    private void Update()
    {
        change = Vector3.zero;
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");

        if (currentState != PlayerState.ATTACK && Input.GetButtonDown("Attack")
            && currentState != PlayerState.STAGGER)
        {
            StartCoroutine(AttackGo());
        }
    }

    private IEnumerator AttackGo()
    {
        animator.SetBool("Attacking", true);
        currentState = PlayerState.ATTACK;
        yield return null;
        animator.SetBool("Attacking", false);
        yield return new WaitForSeconds(0.3f);
        currentState = PlayerState.WALK;
    }

    private void FixedUpdate()
    {
        if (currentState == PlayerState.WALK || currentState == PlayerState.IDLE)
        {
            UpdateAnimationAndMove();
        }
    }

    private void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharacter();
            animator.SetFloat("MoveX", change.x);
            animator.SetFloat("MoveY", change.y);
            animator.SetBool("Moving", true);
        }
        else
        {
            animator.SetBool("Moving", false);
        }
    }

    private void MoveCharacter()
    {
        change.Normalize();
        myRigidbody.MovePosition(
            transform.position + change * speed * Time.fixedDeltaTime
        );
    }

    public void Knock(float knockTime, float damage)
    {
        currentHealth.initialValue -= damage;
        Debug.Log(currentHealth.initialValue);
        if (currentHealth.initialValue > 0)
        {
            playerHealhSignal.Raise();
            StartCoroutine(KnockCo(knockTime));
        }
    }

    private IEnumerator KnockCo(float knockTime)
    {
        if (myRigidbody)
        {
            yield return new WaitForSeconds(knockTime);
            myRigidbody.velocity = Vector2.zero;
            currentState = PlayerState.IDLE;
        }
    }
}
