﻿public enum PlayerState
{
    IDLE,
    WALK,
    ATTACK,
    INTERACT,
    STAGGER
}