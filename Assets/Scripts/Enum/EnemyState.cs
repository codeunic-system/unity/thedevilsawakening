﻿public enum EnemyState
{
    IDLE,
    WALK,
    ATTACK,
    STAGGER
}